PLEASE CAREFULLY REVIEW THE "I/O BUFFER MODEL LICENSE TERMS AND CONDITIONS" BEFORE USING THE DELIVERABLES

Copyright: 2011 Altera Corporation. All rights reserved. Altera, The Programmable Solutions Company, the stylized Altera logo, specific device designations and all other words and logos that are identified as trademarks and/or service marks are, unless noted otherwise, the trademarks and service marks of Altera Corporation in the U.S. and other countries.* All other product or service names are the property of their respective holders. Altera products are protected under numerous U.S. and foreign patents and pending applications, maskwork rights, and copyrights.

Altera Corporation
101 Innovation Drive
San Jose, CA 95134
(408) 544-7000
www.altera.com


Revision history:

Version 0.1
 Initial release

Version 1.0
  Release without modification


